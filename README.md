Goal: 
*Changing background image
*2 buttons that modify the order of the image. (Previews and Next)

Pros:
* Makes the page more dynamic
* It is an essential thing for a showroom / portfolio page.

Cons:
* After a while she starts going crazy
* To use the buttons you have to double click.
* May slow down the page